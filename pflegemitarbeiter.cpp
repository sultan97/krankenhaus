#include "pflegemitarbeiter.h"

PflegeMitarbeiter::PflegeMitarbeiter(string name, string vorname, string gehaltsGruppe, int gehaltsStufe, string klinik) :
    Mitarbeiter(name, vorname, gehaltsGruppe, gehaltsStufe, klinik)
{

}

PflegeMitarbeiter::~PflegeMitarbeiter()
{

}

std::string PflegeMitarbeiter::zeigeDetails()
{
    stringstream ss;
    ss.width(3);
    ss.setf(ios::left);

    ss << to_string(id) << setw(20) << "Pflegemitarbeiter: " << setw(13) << vorname << setw(15) << name << "Gruppe: " << gehaltsGruppe
       << ", Stufe: " << left << setw(30) << to_string(gehaltsStufe) << klinik;
    string detail = ss.str();

    /*string details = to_string(id) + " Arzt: " + vorname + " " + name + ", Gruppe: " + gehaltsGruppe
            + ", Stufe: " + to_string(gehaltsStufe) + ", " + klinik;*/
    return detail;
}
