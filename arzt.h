#ifndef ARZT_H
#define ARZT_H

#include "mitarbeiter.h"

class Arzt : public Mitarbeiter
{
public:
    Arzt(string name, string vorname, string gehaltsGruppe, int gehaltsStufe, string klinik);
    ~Arzt();
    virtual std::string zeigeDetails() override;
};

#endif // ARZT_H
