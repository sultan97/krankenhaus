#ifndef PFLEGEMITARBEITER_H
#define PFLEGEMITARBEITER_H

#include "mitarbeiter.h"

class PflegeMitarbeiter : public Mitarbeiter
{
public:
    PflegeMitarbeiter(string name, string vorname, string gehaltsGruppe, int gehaltsStufe, string klinik);
    ~PflegeMitarbeiter();
    virtual std::string zeigeDetails() override;
};

#endif // PFLEGEMITARBEITER_H
