#include "personalverwaltung.h"

PersonalVerwaltung::PersonalVerwaltung()
{

}

PersonalVerwaltung::~PersonalVerwaltung()
{
    resetData();
}

void PersonalVerwaltung::benutzerDialog()
{
    while (true)
    {
        char eingabe;

        cout << "1. Datei einlesen\n"
             << "2. Mitarbeiter entlassen\n"
             << "3. Liste ausgeben\n"
             << "0. Program beenden\n" << endl;

        cin >> eingabe;

        switch(eingabe)
        {
        case '1': dateiEinlesen();
            break;
        case '2': entlassen();
            break;
        case '3':
        {
            for (unsigned int i = 0; i < mitarbeiterListe.size(); i++)
            {
                cout << mitarbeiterListe[i]->zeigeDetails() << endl;
            }
            cout << endl;
        }
            break;
        case '0': return;
        default: cout << "falsche Eingabe. Bitte erneut versuchen\n" << endl;
            continue;
        }
    }
}

void PersonalVerwaltung::dateiEinlesen()
{
    if (mitarbeiterListe.size() > 0)
    {
        resetData();
    }

    QString dateiName = "mitarbeiter.txt";

    QFile datei(dateiName);

    if (!datei.open(QIODevice::ReadOnly))
    {
        cerr << "Datei konnte nicht geoeffnet werden\n" << endl;
    }

    QString dateiInhalt = datei.readAll().constData();
    datei.close();

    QStringList zeilen = dateiInhalt.split("\r\n");
    QStringList* attribute = new QStringList[zeilen.size()];

    for (int i = 0; i < zeilen.size(); i++)
    {
        attribute[i] = zeilen[i].split(";");
        QString type;
        QString name, vorname, gehaltsGruppe, klinik;
        int gehaltsStufe;

        for (int j = 0; j < attribute[i].size(); j++)
        {
            switch(j)
            {
            case 0: type = attribute[i][j];
                break;
            case 1: name = attribute[i][j];
                break;
            case 2: vorname = attribute[i][j];
                break;
            case 3: gehaltsGruppe = attribute[i][j];
                break;
            case 4: gehaltsStufe = attribute[i][j].toInt();
                break;
            case 5: klinik = attribute[i][j];
                break;
            default: break;
            }
        }

        if (type == "A")
        {
            Arzt* arzt = new Arzt(name.toStdString(), vorname.toStdString(), gehaltsGruppe.toStdString(), gehaltsStufe, klinik.toStdString());
            mitarbeiterListe.push_back(arzt);
        }
        else if (type == "P")
        {
            PflegeMitarbeiter* pflegeMitarbeiter = new PflegeMitarbeiter(name.toStdString(), vorname.toStdString(), gehaltsGruppe.toStdString(), gehaltsStufe, klinik.toStdString());
            mitarbeiterListe.push_back(pflegeMitarbeiter);
        }
    }

    cout << mitarbeiterListe.size() << " Mitarbeiter eingelesen.\n" << endl;
    delete [] attribute;
}

void PersonalVerwaltung::entlassen()
{
    int eingabe;
    bool istGefunden = false;

    cout << "Geben Sie den ID des Mitarbeiters: ";

    cin >> eingabe;

    for (auto it = mitarbeiterListe.begin(); it != mitarbeiterListe.end(); ++it)
    {
        if ((*it)->getId() == eingabe)
        {
            istGefunden = true;
            cout << "Mitarbeiter " << (*it)->getVorname() << " " << (*it)->getName() << " wurde geloescht\n" << endl;
            delete (*it);
            mitarbeiterListe.erase(it);
            break;
        }
    }

    if (istGefunden == false) {
        cout << "kein Mitarbeiter gefunden zu ID " << eingabe << endl << endl;
    }
}

void PersonalVerwaltung::resetData()
{
    for (auto person : mitarbeiterListe)
    {
        delete person;
    }
    Mitarbeiter::setMaxId(1);
    mitarbeiterListe.clear();
}
