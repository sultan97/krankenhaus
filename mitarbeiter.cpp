#include "mitarbeiter.h"

long Mitarbeiter::maxId = 1;

Mitarbeiter::Mitarbeiter(string name, string vorname, string gehaltsGruppe, int gehaltsStufe, string klinik) : name(name), vorname(vorname), gehaltsGruppe(gehaltsGruppe), gehaltsStufe(gehaltsStufe), klinik(klinik)
{
    id = maxId++;
}

Mitarbeiter::~Mitarbeiter()
{

}

void Mitarbeiter::setMaxId(long newMaxId)
{
    maxId = newMaxId;
}

long Mitarbeiter::getId() const
{
    return id;
}

string Mitarbeiter::getVorname() const
{
    return vorname;
}

string Mitarbeiter::getName() const
{
    return name;
}
