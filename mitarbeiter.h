#ifndef MITARBEITER_H
#define MITARBEITER_H

#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>

using namespace std;

class Mitarbeiter
{
public:
    Mitarbeiter(string name, string vorname, string gehaltsGruppe, int gehaltsStufe, string klinik);
    virtual ~Mitarbeiter();

    virtual string zeigeDetails() = 0;

    static void setMaxId(long newMaxId);
    long getId() const;
    string getVorname() const;
    string getName() const;

protected:
    long id;
    static long maxId;
    string gehaltsGruppe;
    int gehaltsStufe;
    string name;
    string vorname;
    string klinik;
};

#endif // MITARBEITER_H
