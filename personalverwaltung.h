#ifndef PERSONALVERWALTUNG_H
#define PERSONALVERWALTUNG_H

#include <QVector>
#include <QString>
#include <QFile>
#include <QIODevice>
#include <QStringList>

#include "mitarbeiter.h"
#include "arzt.h"
#include "pflegemitarbeiter.h"

class PersonalVerwaltung
{
public:
    PersonalVerwaltung();
    ~PersonalVerwaltung();

    void benutzerDialog();

private:
    vector<Mitarbeiter*> mitarbeiterListe;
    void dateiEinlesen();
    void entlassen();
    void resetData();
};

#endif // PERSONALVERWALTUNG_H
